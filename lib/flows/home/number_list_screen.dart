import 'package:flutter/material.dart';

abstract class NumberListScreenListener<T extends StatefulWidget> implements State<T> {
  void onNumberPressed(int number);
}

class NumberListScreen extends StatefulWidget {
  const NumberListScreen({Key? key}) : super(key: key);

  @override
  _NumberListScreenState createState() => _NumberListScreenState();
}

class _NumberListScreenState extends State<NumberListScreen> {
  NumberListScreenListener? _screenListener;

  @override
  void initState() {
    super.initState();
    _screenListener = context.findAncestorStateOfType<NumberListScreenListener>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView.separated(
        itemCount: 15,
        itemBuilder: (context, index) => ListTile(
          title: Text('$index'),
          onTap: () => _screenListener?.onNumberPressed(index),
        ),
        separatorBuilder: (_, __) => const Divider(height: 0),
      ),
    );
  }
}
