import 'package:flutter/material.dart';

class NumberDetailScreen extends StatelessWidget {
  final int number;

  const NumberDetailScreen({Key? key, required this.number}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(child: Text('The selected number is: $number')),
    );
  }
}
