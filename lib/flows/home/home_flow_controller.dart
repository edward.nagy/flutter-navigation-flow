import 'package:flutter/material.dart';

import '../../base/flow_controller.dart';
import 'number_detail_screen.dart';
import 'number_list_screen.dart';

class HomeFlowController extends StatefulWidget {
  const HomeFlowController({Key? key}) : super(key: key);

  @override
  _HomeFlowControllerState createState() => _HomeFlowControllerState();
}

class _HomeFlowControllerState extends FlowControllerState<HomeFlowController>
    implements NumberListScreenListener<HomeFlowController> {
  @override
  Page get initialPage => const MaterialPage(child: NumberListScreen());

  @override
  void onNumberPressed(int number) {
    navigator.push(MaterialPage(child: NumberDetailScreen(number: number)));
  }
}
