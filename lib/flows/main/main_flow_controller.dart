import 'package:flutter/material.dart';

import '../../base/flow_controller.dart';
import '../home/home_flow_controller.dart';
import '../profile/profile_screen.dart';
import 'main_screen.dart';

class MainFlowController extends StatefulWidget {
  const MainFlowController({Key? key}) : super(key: key);

  @override
  _MainFlowControllerState createState() => _MainFlowControllerState();
}

class _MainFlowControllerState extends FlowControllerState<MainFlowController>
    implements MainScreenListener<MainFlowController> {
  @override
  Page get initialPage => const MaterialPage(child: MainScreen());

  @override
  Widget buildHomeScreen(BuildContext context) {
    return const HomeFlowController();
  }

  @override
  Widget buildProfileScreen(BuildContext context) {
    return const ProfileScreen();
  }
}
