import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'flows/root/root_flow_controller.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // this Router is needed to obtain the [BackButtonDispatcher]
    return MaterialApp.router(
      title: 'Navigation Flow',
      routerDelegate: AppRouterDelegate(home: const RootFlowController()),
      routeInformationParser: AppRouteInformationParser(),
    );
  }
}

class AppRouterDelegate extends RouterDelegate<AppPath> with ChangeNotifier, PopNavigatorRouterDelegateMixin<AppPath> {
  final Widget home;

  AppRouterDelegate({required this.home});

  @override
  Widget build(BuildContext context) {
    return home;
  }

  @override
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey();

  @override
  Future<void> setNewRoutePath(configuration) {
    // TODO: implement setNewRoutePath
    return SynchronousFuture(null);
  }
}

class AppRouteInformationParser extends RouteInformationParser<AppPath> {
  @override
  Future<AppPath> parseRouteInformation(RouteInformation routeInformation) {
    // TODO: implement parseRouteInformation
    return SynchronousFuture(AppPath());
  }
}

class AppPath {}
