import 'package:flutter/material.dart';

import '../../base/flow_controller.dart';
import '../auth/auth_flow_controller.dart';
import '../auth/sign_in_screen.dart';
import '../auth/sign_up_screen.dart';
import '../main/main_flow_controller.dart';
import 'splash_screen.dart';

class RootFlowController extends StatefulWidget {
  const RootFlowController({Key? key}) : super(key: key);

  @override
  _RootFlowControllerState createState() => _RootFlowControllerState();
}

class _RootFlowControllerState extends FlowControllerState<RootFlowController>
    implements SignInScreenListener<RootFlowController>, SignUpScreenListener<RootFlowController> {
  @override
  Page get initialPage => const MaterialPage(child: SplashScreen());

  @override
  void initState() {
    super.initState();
    _loadInitialPage();
  }

  @override
  void onUserSignedIn() {
    navigator.replaceAll(const MaterialPage(child: MainFlowController()));
  }

  @override
  void onUserSignedUp() {
    navigator.replaceAll(const MaterialPage(child: MainFlowController()));
  }

  Future<void> _loadInitialPage() async {
    final isAuthenticated = await _isAuthenticated();
    if (isAuthenticated) {
      navigator.replace(const MaterialPage(child: MainFlowController()));
    } else {
      navigator.replace(const MaterialPage(child: AuthFlowController()));
    }
  }
}

Future<bool> _isAuthenticated() async {
  await Future.delayed(const Duration(milliseconds: 2000));
  return false;
}
