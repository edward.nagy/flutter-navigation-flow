import 'package:flutter/material.dart';

abstract class MainScreenListener<T extends StatefulWidget> implements State<T> {
  Widget buildHomeScreen(BuildContext context);
  Widget buildProfileScreen(BuildContext context);
}

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  late final MainScreenListener _screenListener;

  @override
  void initState() {
    super.initState();
    _screenListener = context.findAncestorStateOfType<MainScreenListener>()!;
  }

  int _activeIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _activeIndex,
        children: [
          _screenListener.buildHomeScreen(context),
          _screenListener.buildProfileScreen(context),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _activeIndex,
        onTap: (index) => setState(() {
          _activeIndex = index;
        }),
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profile',
          ),
        ],
      ),
    );
  }
}
