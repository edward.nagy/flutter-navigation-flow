import 'package:flutter/material.dart';

abstract class AuthScreenListener<T extends StatefulWidget> implements State<T> {
  void onSignInButtonPressed();
  void onSignUpButtonPressed();
}

class AuthScreen extends StatefulWidget {
  const AuthScreen({Key? key}) : super(key: key);

  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  AuthScreenListener? _screenListener;

  @override
  void initState() {
    super.initState();
    _screenListener = context.findAncestorStateOfType<AuthScreenListener>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ElevatedButton(
              onPressed: _screenListener?.onSignInButtonPressed,
              child: const Text('Sign In'),
            ),
            ElevatedButton(
              onPressed: _screenListener?.onSignUpButtonPressed,
              child: const Text('Sign Up'),
            ),
          ],
        ),
      ),
    );
  }
}
