import 'package:flutter/material.dart';

abstract class SignInScreenListener<T extends StatefulWidget> implements State<T> {
  void onUserSignedIn();
}

class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  SignInScreenListener? _screenListener;

  @override
  void initState() {
    super.initState();
    _screenListener = context.findAncestorStateOfType<SignInScreenListener>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            const TextField(
              decoration: InputDecoration(labelText: 'Email'),
            ),
            const SizedBox(height: 8.0),
            const TextField(
              decoration: InputDecoration(labelText: 'Password'),
            ),
            const SizedBox(height: 8.0),
            ElevatedButton(
              onPressed: _screenListener?.onUserSignedIn,
              child: const Text('Sign In'),
            )
          ],
        ),
      ),
    );
  }
}
