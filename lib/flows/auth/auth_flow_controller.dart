import 'package:flutter/material.dart';

import '../../base/flow_controller.dart';
import 'auth_screen.dart';
import 'sign_in_screen.dart';
import 'sign_up_screen.dart';

class AuthFlowController extends StatefulWidget {
  const AuthFlowController({Key? key}) : super(key: key);

  @override
  _AuthFlowControllerState createState() => _AuthFlowControllerState();
}

class _AuthFlowControllerState extends FlowControllerState<AuthFlowController>
    implements AuthScreenListener<AuthFlowController> {
  @override
  Page get initialPage => const MaterialPage(child: AuthScreen());

  @override
  void onSignInButtonPressed() {
    navigator.push(const MaterialPage(child: SignInScreen()));
  }

  @override
  void onSignUpButtonPressed() {
    navigator.push(const MaterialPage(child: SignUpScreen()));
  }
}
