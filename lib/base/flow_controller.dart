import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

abstract class FlowControllerState<T extends StatefulWidget> extends State<T> {
  late final FlowNavigator navigator;
  late final FlowRouterDelegate _routerDelegate;
  late BackButtonDispatcher _backButtonDispatcher;

  Page get initialPage;

  @override
  void initState() {
    super.initState();
    navigator = FlowNavigator(initialPage);
    _routerDelegate = FlowRouterDelegate(navigator);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // Defer back button dispatching to the child router
    _backButtonDispatcher = Router.of(context).backButtonDispatcher!.createChildBackButtonDispatcher();
  }

  @override
  Widget build(BuildContext context) {
    // Claim priority. If there are parallel sub routers, you will need to pick which one should take priority.
    _backButtonDispatcher.takePriority();
    return Router(
      routerDelegate: _routerDelegate,
      backButtonDispatcher: _backButtonDispatcher,
    );
  }
}

class FlowNavigator extends ChangeNotifier {
  final List<Page> _pages;

  FlowNavigator(Page initialPage) : _pages = [initialPage];

  // It's important to provide new list for Navigator each time
  // because it compares previous list with the next one on each [NavigatorState didUpdateWidget].
  List<Page> get pages => List.unmodifiable(_pages);

  bool canPop() => _pages.length > 1;

  void pop() {
    if (canPop()) {
      _pages.removeLast();
      notifyListeners();
    }
  }

  void push(Page page) {
    _pages.add(page);
    notifyListeners();
  }

  void replace(Page page) {
    if (_pages.isNotEmpty) {
      _pages.removeLast();
    }
    _pages.add(page);
    notifyListeners();
  }

  void replaceAll(Page page) {
    setPages([page]);
  }

  void setPages(List<Page> pages) {
    _pages.clear();
    _pages.addAll(pages);
    notifyListeners();
  }
}

class FlowRouterDelegate extends RouterDelegate with ChangeNotifier, PopNavigatorRouterDelegateMixin {
  FlowNavigator navigator;

  @override
  final GlobalKey<NavigatorState> navigatorKey;

  FlowRouterDelegate(this.navigator) : navigatorKey = GlobalKey() {
    navigator.addListener(notifyListeners);
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      pages: navigator.pages,
      onPopPage: _onPopPage,
    );
  }

  bool _onPopPage(Route route, result) {
    final didPop = route.didPop(result);
    if (!didPop) {
      return false;
    }
    if (navigator.canPop()) {
      navigator.pop();
      return true;
    } else {
      return false;
    }
  }

  @override
  Future<void> setNewRoutePath(dynamic configuration) {
    // Route parsing is not done by the inner routers.
    assert(false);
    return SynchronousFuture(null);
  }
}
