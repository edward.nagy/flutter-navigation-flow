import 'package:flutter/material.dart';

abstract class SignUpScreenListener<T extends StatefulWidget> implements State<T> {
  void onUserSignedUp();
}

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  SignUpScreenListener? _screenListener;

  @override
  void initState() {
    super.initState();
    _screenListener = context.findAncestorStateOfType<SignUpScreenListener>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            const TextField(
              decoration: InputDecoration(labelText: 'Email'),
            ),
            const SizedBox(height: 8.0),
            const TextField(
              decoration: InputDecoration(labelText: 'Password'),
            ),
            const SizedBox(height: 8.0),
            ElevatedButton(
              onPressed: _screenListener?.onUserSignedUp,
              child: const Text('Sign Up'),
            )
          ],
        ),
      ),
    );
  }
}
